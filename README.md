# edgar-logger

## Description

A very simple logger library for all the Edgar projects (app, instrumenter, collector, reporter, memoizer, etc.)

## Usage
Developers will use the `edgar-logger` library project to create and populate log files in short bursts (when using the
Edgar app or one of its components) or in long-running programs like the `edgar-memoizer`.
The log files are of the form `log-<datestamp>` where datestamp is an easily readable value using a date format.
A separate log file is created for each day a message is logged.

Standard developer tools like `grep` or text editors can be used to extract information from the log files.

Developers will use an object (named Log) to perform these two operations, create and populate, with the following
methods:

`start(dirPath: File = getDefaultDir(), message: String = "")` will create a log file in a given
directory, creating the directory if needed and throwing an exception if the log file cannot be created.
The default directory resolves to `~/.config/edgar/logs`, but using separate directories for each app or library is
highly encouraged.

The log file name is of the form: log-1970-01-01.txt

An optional start message can be supplied. The default start message is of the form: Logging started on Thursday,
January 1, 1970. 

`add(message: String, timestamp: Instant = Instant.now())` will append `message` to the end of the current log file.
If there is no current log file, i.e. no start operation was performed, one will be created using the default location.
The `timestamp` argument defaults to now and allows for better testing.

Each message will be of the form: 12:01:23.537981Z something important happened.

## Building

`./gradlew clean build` is the clean approach to building and testing.
A successful build means all tests pass, all code is covered by tests and there are no warnings.
Hence, the project is ready for release.

## Releasing

Commit and push the project files. Please ensure that all stale commented out code has been removed.
Also, please make sure the version has been bumped (usually at the start of a feature or bug fix development session).

## Deploying

Deploying consists of registering a released library with Maven Central using one of the Gradle Publish plugin tasks.
Only a designated project lead can deploy. Deploying uses a special script, `publish.sh` which is kept well hidden to
protect the privacy of the Edgar secret key. Note that this script automagically deploys to Maven Central.
