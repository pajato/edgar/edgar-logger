package com.pajato.edgar.logger

import com.pajato.edgar.logger.Log.createDirIfNecessary
import com.pajato.edgar.logger.Log.getDefaultStartMessage
import com.pajato.edgar.logger.Log.getLogName
import com.pajato.edgar.logger.Log.logFile
import java.io.File
import java.io.IOException
import java.time.Instant
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class LogTest {
    private val path = "build/logs"
    private val testLogDir = File(System.getProperty("user.dir"), path).also { file -> file.mkdirs() }
    private val testLogFile = File(testLogDir, getLogName())

    @BeforeTest fun setUp() {
        logFile = null
        testLogFile.writeText("")
    }

    @Test fun `When dealing with a timestamp, verify the name and day`() {
        val logFile = LogFile(testLogDir, Instant.ofEpochSecond(0L))
        assertEquals(logFile.name, "log-1970-01-01.txt")
        assertEquals(logFile.createDay, "Thursday, January 1, 1970")
    }

    @Test fun `When trying to simulate a mkdirs() failure, verify an IO exception is thrown`() {
        val notADirectory = File(testLogDir, "notADirectory").also { file -> file.createNewFile() }
        assertFailsWith<IOException> { notADirectory.also { file -> createDirIfNecessary(file) } }
    }

    @Test fun `When the logger is started using the default log path, verify default message`() {
        Log.start()
        when {
            logFile == null -> fail("Log file is null!")
            logFile!!.current.readText().isEmpty() -> fail("Empty log file!")
            else -> assertTrue(logFile!!.current.readText().contains(getDefaultStartMessage()))
        }
    }

    @Test fun `When the logger is started using the test log path, verify message`() {
        val message = "Testing non-default start message..."
        Log.start(testLogDir, message)
        assertTrue(testLogFile.readText().contains(message))
    }

    @Test fun `When a log entry is added without starting the logger, verify code coverage`() {
        Log.add("some log message")
    }

    @Test fun `When a date has been crossed, verify new name`() {
        val time: Instant = Instant.ofEpochMilli(Instant.now().toEpochMilli() - 24L * 60 * 60 * 1000)
        val name = getLogName(time = time)
        logFile = LogFile(dir = testLogDir, time)
        Log.add("Testing date crossing")
        assertNotEquals(logFile!!.name, name)
    }

    @Test fun `When a path needs to have directories created, verify for coverage`() {
        createDirIfNecessary(File(testLogDir, "no/such/path"))
    }

    @Test fun `When the log file has not been initialized, verify empty content`() {
        assertEquals("", Log.content())
    }

    @Test fun `When the log file has been initialized, verify the content`() {
        val startLogMessage = "xyzzy"
        Log.start(testLogDir, startLogMessage)
        assertTrue(Log.content().contains(startLogMessage))
    }
}
