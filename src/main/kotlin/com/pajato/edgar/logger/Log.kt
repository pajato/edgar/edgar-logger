package com.pajato.edgar.logger

import java.io.File
import java.io.IOException
import java.time.Instant
import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal const val createErrorMessage = "Could not create the log file!"
internal const val restartLogErrorMessage = "The logger can only be started once!."

internal fun getDirCreateMessage(dir: File) = "Could not create log directory: ${dir.absolutePath} !"

internal class LogFile(val dir: File, timestamp: Instant = Instant.now()) {
    val name = Log.getLogName(timestamp)
    val createDay = getLogCreateDay(timestamp)
    val current = File(dir, name)

    private fun getLogCreateDay(timestamp: Instant): String {
        val date = LocalDate.parse(timestamp.toString().split("T")[0])
        val formatter = DateTimeFormatter.ofPattern("EEEE, MMMM d, YYYY")
        return formatter.format(date)
    }
}

object Log {
    internal var logFile: LogFile? = null

    /**
     * Append a message to the current log file, creating one if necessary.
     *
     * @param message the message to append.
     * @param timestamp the value to use as a prefix for the message in the log. Defaults to now.
     */
    fun add(message: String, timestamp: Instant = Instant.now()) {
        if (logFile == null) start()
        if (dateHasCrossed())
            start(logFile!!.dir)
        logFile!!.current.appendText("${timestamp.toString().split("T")[1]} $message\n")
    }

    /**
     * Activate a new logging session.
     *
     * @param dir A valid file that exists and is a directory. Defaults to ~/.config/edgar/logs
     * @param message An optional message to flag the start of a session.
     */
    fun start(dir: File = getDefaultDir(), message: String = "") {
        createDirIfNecessary(dir)
        logFile = LogFile(dir)
        add(message.ifEmpty { getDefaultStartMessage() })
    }

    /**
     * Return the current content of the log file.
     */
    fun content(): String {
        val file = logFile ?: return ""
        return file.current.readText()
    }

    internal fun getDefaultStartMessage() = "Logging started on ${logFile!!.createDay}."

    internal fun createDirIfNecessary(dir: File) {
        fun isValid(dir: File) = (dir.exists() && dir.isDirectory) || (!dir.exists() && dir.mkdirs())

        if (isValid(dir)) return else throw IOException(getDirCreateMessage(dir))
    }

    internal fun getLogName(time: Instant = Instant.now()) = "log-${time.toString().split("T")[0]}.txt"

    private fun getDefaultDir() =
        File(System.getProperty("user.home"), ".config/edgar/logs").also { dir -> createDirIfNecessary(dir) }

    private fun dateHasCrossed(): Boolean = logFile!!.name != getLogName()
}
